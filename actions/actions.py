
from typing import Text, Dict, Any, List
from rasa_sdk import Action
from rasa_sdk import Tracker                                               
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet
import requests
from string import Template
import os

class GetAccountBalance(Action):
    def name(self) -> Text:
        return "action_get_account_balance"

    def get_account_balance(self, user_dni, account_type, account_currency):
        # connect to Aconcagua API 
        CuentaClienteTemplate = '''query {
            CuentaCliente(NumDoc: "$user_dni") {
                Saldo
            }
        }'''

        query=Template(CuentaClienteTemplate).substitute(user_dni=user_dni)

        try:
            response = requests.post(os.environ['GRAPHQL_ENDPOINT'], json={'query': query})
            print(response.json())
            try:
                saldo = response.json()['data']['CuentaCliente']['Saldo']
                return f'Tu saldo es %s.'%(saldo)
            except:
                return f'No se pudo obtener el saldo del DNI %s, seguro que la cuenta existe?'%(user_dni)
        except requests.exceptions.RequestException as e:
            print(e)
            return 'No se pudo obtener conectar con el servicio de Aconcagua'


    async def run(
        self,
        dispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:

        user_dni = tracker.get_slot("user_dni")
        account_type = tracker.get_slot("account_type")
        account_currency = tracker.get_slot("account_currency")

        account_balance_response = self.get_account_balance(user_dni, account_type, account_currency)

        dispatcher.utter_message(
            account_balance_response
        )

        return []

class GetBranches(Action):
    def name(self) -> Text:
        return "action_get_branches"

    def get_branches(self): 
                # get the branches
        branches = [
            "Branch 1",
            "Branch 2",
            "Branch 3",
            "Branch 4",
            "Branch 5",
        ]

        return branches

    async def run(
        self,
        dispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:


        branches = self.get_branches()

        dispatcher.utter_message("Your branches are: {}".format(branches))

        return []       
